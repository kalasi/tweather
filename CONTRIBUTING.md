### Bugs

Make an issue, be sure to include the terminal output if there is any.

### Pull Requests

Submit to the 'develop' branch. Any and all are welcome. Support for other
regions or countries is welcome as well.

### Feature Requests

Make an issue and it'll be resolved Soon(tm).
