[![ci-badge][]][ci] [![license-badge][]][license]

# tweather

Quick and easy data about weather at your location via the terminal.

### Installation

1. `$ git clone https://gitlab.com/kalasi/tweather.git`
2. `$ cd tweather`
3. `$ cargo build --release`
4. Move the binary at `./target/releases/tweather` to a place of your choosing.
   This could be to `/usr/local/bin/tweather`.

You may also want to add an alias to your .bashrc or equivilant, like so:

```sh
alias weather="tweather LATITUDE_VALUE LONGITUDE_VALUE"
```

### Usage

Simply pass the latitude and longitude of your location to tweather, like so:

```sh
$ tweather LATITUDE_VALUE LONGITUDE_VALUE
```

For example, the following is New York City Times Square's data:

```sh
$ tweather 40.75773 -73.985708
```

### Contributing

See the [CONTRIBUTING.md file].

### License

See the [LICENSE.md file].

[ci]: https://gitlab.com/kalasi/tweather/pipelines
[ci-badge]: https://gitlab.com/kalasi/tweather/badges/master/build.svg
[CONTRIBUTING.md file]: https://gitlab.com/kalasi/tweather/blob/master/CONTRIBUTING.md
[LICENSE.md file]: https://github.com/zeyla/tweather/blob/master/LICENSE.md
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square
[license]: https://opensource.org/licenses/ISC
