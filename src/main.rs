// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

//! Quick and easy data about weather at your location via the terminal.
//!
//!
//! ### Installation
//!
//! 1. `$ git clone https://github.com/zeyla/tweather.git`
//! 2. `$ cd tweather`
//! 3. `$ cargo build --release`
//! 4. move the binary at `./target/releases/tweather` to a place of your choosing.
//!
//! You may also want to add an alias to your .bashrc or equivilant, like so:
//!
//! ```sh
//! alias weather="tweather LATITUDE_VALUE LONGITUDE_VALUE"
//! ```
//!
//!
//! ### Usage
//!
//! Simply pass the latitude and longitude of your location to tweather, like so:
//!
//! ```sh
//! $ tweather LATITUDE_VALUE LONGITUDE_VALUE
//! ```
//!
//! For example, the following is New York City Times Square's data:
//!
//! ```sh
//! $ tweather 40.75773 -73.985708
//! ```

// Some struct fields are not in any sort of naming scheme that makes sense.
// Blame weather.gov for that.
//
// The URL used to retrieve data:
// http://forecast.weather.gov/MapClick.php?lat={}&lon={}&FcstType=json
// where the string is formatted for the latitude and longitude given in the
// arguments.
#![allow(non_snake_case)]

extern crate hyper;
extern crate rustc_serialize;

mod retrieve;
mod structs;

use std::env::args;
use structs::{Coordinates, WeatherGovData};

fn main() {
    let arg_latitude: f64 = match args().nth(1) {
        Some(arg) => {
            &arg[..]
        }.parse::<f64>().unwrap(),
        None => panic!("A latitude must be given."),
    };
    let arg_longitude: f64 = match args().nth(2) {
        Some(arg) => {
            &arg[..]
        }.parse::<f64>().unwrap(),
        None => panic!("A longitude must be given."),
    };
    let coords: Coordinates = Coordinates {
        latitude: arg_latitude,
        longitude: arg_longitude,
    };

    let data: WeatherGovData = retrieve::get(&coords);

    let data_points: usize = data.time.startPeriodName.len();

    println!("Time: {}", data.currentobservation.Date);
    println!("Currently: {}F", data.currentobservation.Temp);
    println!("Looks like: {}", data.currentobservation.Weather);

    for x in 0..data_points {
        let temp: &str = data.data.temperature.get(x).unwrap();
        let time: &str = data.time.startPeriodName.get(x).unwrap();
        let weather: &str = data.data.weather.get(x).unwrap();
        println!("{}: {} ({})", time, temp, weather);
    }
}
