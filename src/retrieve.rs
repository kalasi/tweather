// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use hyper::header::{Headers, UserAgent};
use hyper::Client;
use rustc_serialize::json;
use std::io::Read;
use std::str;
use structs::{Coordinates, WeatherGovData};

pub fn get(coords: &Coordinates) -> WeatherGovData {
    let url: String = format!("http://forecast.weather.gov/MapClick.php?\
                              lat={}&lon={}&FcstType=json",
                              coords.latitude,
                              coords.longitude);

    let body = {
        // Emulate a user agent since weather.gov rejects the request otherwise
        // and outputs a 403.
        let user_agent = "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0)\
                          Gecko/41.0 Firefox/41.0".to_string();
        let client = Client::new();
        let mut headers = Headers::new();
        headers.set(UserAgent(user_agent));

        let mut res = client.get(&url)
            .headers(headers)
            .send()
            .unwrap();

        let mut s = String::new();
        res.read_to_string(&mut s).ok();

        s
    };

    let decoded: WeatherGovData = json::decode(&body).unwrap();

    decoded
}
