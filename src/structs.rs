// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use std::borrow::Cow;

#[derive(Clone, Debug, RustcDecodable, RustcEncodable)]
pub struct Coordinates {
    pub latitude: f64,
    pub longitude: f64,
}

#[derive(Clone, Debug, RustcDecodable, RustcEncodable)]
pub struct CurrentObservation<'a> {
    pub Altimeter: Cow<'a, str>,
    pub Date: Cow<'a, str>,
    pub Dewp: Cow<'a, str>,
    pub elev: Cow<'a, str>,
    pub id: Cow<'a, str>,
    pub latitude: Cow<'a, str>,
    pub longitude: Cow<'a, str>,
    pub name: Cow<'a, str>,
    pub Relh: Cow<'a, str>,
    pub SLP: Cow<'a, str>,
    pub state: Cow<'a, str>,
    pub Temp: Cow<'a, str>,
    pub timezone: Cow<'a, str>,
    pub Visibility: Cow<'a, str>,
    pub Weather: Cow<'a, str>,
    pub Weatherimage: Cow<'a, str>,
    pub WindChill: Cow<'a, str>,
    pub Winds: Cow<'a, str>,
}

#[derive(Clone, Debug, RustcDecodable, RustcEncodable)]
pub struct Data<'a> {
    pub hazardUrl: Vec<Cow<'a, str>>,
    pub hazard: Vec<Cow<'a, str>>,
    pub iconLink: Vec<Cow<'a, str>>,
    pub pop: Vec<Option<Cow<'a, str>>>,
    pub temperature: Vec<Cow<'a, str>>,
    pub text: Vec<Cow<'a, str>>,
    pub weather: Vec<Cow<'a, str>>,
}

#[derive(Clone, Debug, RustcDecodable, RustcEncodable)]
pub struct Location<'a> {
    pub areaDescription: Cow<'a, str>,
    pub county: Cow<'a, str>,
    pub elevation: Cow<'a, str>,
    pub firezone: Cow<'a, str>,
    pub latitude: Cow<'a, str>,
    pub longitude: Cow<'a, str>,
    pub radar: Cow<'a, str>,
    pub region: Cow<'a, str>,
    pub timezone: Cow<'a, str>,
    pub wfo: Cow<'a, str>,
    pub zone: Cow<'a, str>,
    pub metar: Cow<'a, str>,
}

#[derive(Clone, Debug, RustcDecodable, RustcEncodable)]
pub struct Time<'a> {
    pub layoutKey: Cow<'a, str>,
    pub startPeriodName: Vec<Cow<'a, str>>,
    pub startValidTime: Vec<Cow<'a, str>>,
    pub tempLabel: Vec<Cow<'a, str>>,
}

#[derive(Clone, Debug, RustcDecodable, RustcEncodable)]
pub struct WeatherGovData<'a> {
    pub creationDateLocal: Cow<'a, str>,
    pub creationDate: Cow<'a, str>,
    pub credit: Cow<'a, str>,
    pub currentobservation: CurrentObservation<'a>,
    pub data: Data<'a>,
    pub location: Location<'a>,
    pub moreInformation: Cow<'a, str>,
    pub operationalMode: Cow<'a, str>,
    pub srsName: Cow<'a, str>,
    pub time: Time<'a>,
}
